package CGIJenkinsBatch.CalculatorAPI;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCalculatorAPI
{
	CalculatorAPI C;
	int Res;
	@BeforeClass
	public void StartTest()
	{
		C=new CalculatorAPI();
	}
	
	@BeforeMethod
	public void InitRes()
	{
		Res=0;
	}

	@Test(priority=1)
	public void TestAdditionWithPositiveValues()
	{
		Res=C.Addition(20, 30);
		Assert.assertEquals(Res, 50,"Addition function not working with Positive numbers");
	}
	
	@Test(priority=2)
	public void TestAdditionWithZeroValues()
	{
		Res=C.Addition(100, 0);
		Assert.assertEquals(Res, 100,"Addition function not working if 1 of the argument is 0");
	}
	
	
	@Test(priority=3)
	public void TestMultiplicationWithPositiveValues()
	{
		Res=C.Multiplication(20, 30);
		Assert.assertEquals(Res, 600,"Multiplication function not working with Positive numbers");
	}
	
	@Test(priority=4)
	public void TestMultiplicationWithZeroValues()
	{
		Res=C.Multiplication(100, 0);
		Assert.assertEquals(Res, 0,"Multiplication function not working if 1 of the argument is 0");
	}

	@Test(priority=5)
	public void TestDivisionWithPositiveValues()
	{
		Res=C.Division(10, 5);
		Assert.assertEquals(Res, 1,"Division function not working if 1 of the argument is 0");
	}
}
